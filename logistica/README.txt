			         README
			         ======

	          Sistema de Logistica
	      ------------------------------

Bem vindo ao sistema de logística. Este sistema foi desenvolvido
como parte de um desafio do processo de seleção da empresa CI&T.

DETALHES TÉCNICO

O sistema foi desenvolvido na linguagem Java 1.7 e utiliza maven
para resolver as dependências de bibliotecas java do projeto, bem 
como para compilar e rodar o sistema. As interface de acesso ao
usuário foram desenvolvidas utilizando a tecnologia de webservices
restful e todas as mensagens utilizam json.

Para armazenamento dos dados informados, foi utilizado o banco de
dados HSQLDB. Ele irá armazenar as informações em forma de arquivos
e os mesmos serão criados no diretório de execução do projeto, dentro
da pasta "data". Para mudar o banco de dados, basta configurar o 
arquivo application.properties, localizado dentro da pasta:

<DIRETORIO-INSTALACAO>/logistica/src/main/resource/application.properties.

O projeto deve ser compilado novamente após a alteração.

- Frameworks
Spring Boot: Responsável por configurar todas as dependências
de um projeto spring e prover um ambiente de runtime para rodar o 
sistema. Os principais módulos utilizados são:
	> Spring core;
	> Spring data;
	> Spring web;
	> Jackson (json).

hsqldb: Banco de dados responsável pelo armazenamento das informações 
do projeto.

REQUISITOS:

 1. Java versão 7+;
 2. Maven;
 3. Acesso a internet.
 

INSTALAÇÃO:

 1. Descompacte o arquivo logistica.zip
 2. Acesse a pasta destino do projeto logística e execute o seguinte 
 	comando para compilar o sistema:
 	
 	mvn clean compile
 	
 3. Para rodar os testes e deteminar se está tudo ok antes da 
 	execução, rode o seguinte comando:
 	
 	mvn test
 	
 4. Para rodar o sistema, execute o seguinte comando:
 
 	mvn spring-boot:run
 	
INTERFACES REST:
	
Com o sistema executando, as seguintes interfaces estarão disponíveis
para os usuários:

- ADD MALHA
A interface addMalha, deve ser utilizada para carregar uma malha de
rotas no sistema. 
	endpoint: http://<host>:8080/data/addMalha POST
	
O exemplo a seguir, demonstra como deve ser o corpo da mensagem:

{
    "nome": "malha",
    "rotas": [
        {
            "inicioRota": "A",
            "fimRota": "B",
            "distanciaRota": 10
        },
        {
            "inicioRota": "B",
            "fimRota": "D",
            "distanciaRota": 15
        },
        {
            "inicioRota": "A",
            "fimRota": "C",
            "distanciaRota": 20
        },
        {
            "inicioRota": "C",
            "fimRota": "D",
            "distanciaRota": 30
        },
        {
            "inicioRota": "B",
            "fimRota": "E",
            "distanciaRota": 50
        },
        {
            "inicioRota": "D",
            "fimRota": "E",
            "distanciaRota": 30
        }
    ]
}

- GET MALHAS
A interface getMalhas, deve ser utilizada para buscar todas as 
malhas cadastradas no sistema. 
	endpoint: http://<host>:8080/data/getMalhas GET

- GET MALHA
A interface getMalha, deve ser utilizada para buscar uma determinada
malha cadastrada no sistema pelo seu ID. 
	endpoint: http://<host>:8080/data/getMalha/<id> GET
	
- CALCULO ROTA
A interface calculoRota, deve ser utilizada para calcular os custos e a
distância envolvida em um trajeto de uma rota. Possui 2 interfaces para 
calculo, uma interface utiliza a primeira malha encontrada no banco 
de dados e a segunda calcula a rota de acordo com o ID da malha informado
	endpoint1: http://<host>:8080/data/calculoRota/ POST
	endpoint2: http://<host>:8080/data/calculoRota/<id> POST	
	
O exemplo a seguir, demonstra como deve ser o corpo da mensagem:

{
	"pontoOrigem": "A",
	"pontoDestino": "D",
	"autonomia": 10,
	"valorLitro": 2.50
}

Existem outras interfaces disponíveis, porém estas são as principais
para a resolução do desafio proposto.

MOTIVAÇÃO:

Utilizei estes frameworks e arquitetura pois são rápidos para criar
o ambiente de desenvolvimento e provem toda a infraestrutura
necessária para rodar o sistema. Já conhecia os frameworks utilizados
no projeto, porém não havia utilizado até o momento o Spring-boot.
Utiliza-lo foi uma surpresa agradável, pois adicionou rapidez no 
desenvolvimento como um todo. 
Utilizei interfaces restful por ser amplamente utilizada no mercado e de
fácil integração com outros sistemas. O banco de dados HSQLDB foi utilizado
por ser simples, leve e integra perfeitamente com o framework spring data.

DESAFIO:

Entregando Mercadorias

A CI&T esta desenvolvendo um novo sistema de logística e sua ajuda é 
muito importante neste momento. Sua tarefa será desenvolver o novo 
sistema de entregas visando sempre o menor custo. Para popular sua 
base de dados o sistema precisa expor um Webservices que aceite o 
formato de malha logística (exemplo abaixo), nesta mesma requisição o 
requisitante deverá informar um nome para este mapa. É importante que 
os mapas sejam persistidos para evitar que a cada novo deploy todas as 
informações desapareçam. O formato de malha logística é bastante simples, 
cada linha mostra uma rota: ponto de origem, ponto de destino e distância 
entre os pontos em quilômetros.

A B 10
B D 15
A C 20
C D 30
B E 50
D E 30

Com os mapas carregados o requisitante irá procurar o menor valor de entrega e 
seu caminho, para isso ele passará o nome do ponto de origem, nome do ponto de 
destino, autonomia do caminhão (km/l) e o valor do litro do combustível, agora 
sua tarefa é criar este Webservices. Um exemplo de entrada seria, origem A, 
destino D, autonomia 10, valor do litro 2,50; a resposta seria a rota A B D 
com custo de 6,25.

Você está livre para definir a melhor arquitetura e tecnologias para solucionar 
este desafio, mas não se esqueça de contar sua motivação no arquivo README que 
deve acompanhar sua solução, junto com os detalhes de como executar seu programa. 
Documentação e testes serão avaliados também =) Lembre-se de que iremos executar 
seu código com malhas beeemm mais complexas, por isso é importante pensar em 
requisitos não funcionais também!!

Também gostaríamos de acompanhar o desenvolvimento da sua aplicação através do 
código fonte. Por isso, solicitamos a criação de um repositório que seja compartilhado 
conosco. Para o desenvolvimento desse sistema, nós solicitamos que você utilize a 
sua (ou as suas) linguagem de programação principal. Pode ser Java, JavaScript ou Ruby.


Instruções
    Desenvolver um programa conforme o desafio detalhado abaixo
    Fique a vontade para desenvolver uma aplicação Web, Command Line ou Swing-like
    Importante que o código do algoritmo seja Java. Caso opte por uma interface Web 
    o algoritmo não poderá ser escrito em JavaScript no Client-Side e sim em Java 
    no Server-Side.
    Enviar o código em um arquivo zipado.
    Adicionar no pacote zipado, um arquivo README.txt com instruções de como compilar 
    e executar o código enviado.
    Considere utilizar boas práticas de desenvolvimento de software como TDD ou BDD
    Considere implementar os seguintes requisitos de segurança:
        - aplicação deverá ser segura;
        - aplicação deverá ter o rastreamento do que o usuário realizou no sistema.
