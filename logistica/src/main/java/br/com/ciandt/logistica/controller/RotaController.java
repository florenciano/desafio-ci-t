package br.com.ciandt.logistica.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.ciandt.logistica.component.IRotaService;
import br.com.ciandt.logistica.dto.RotaDTO;

/**
 * Controlador responsavel pelas servico de rota
 * 
 * @author Rodrigo Florenciano
 *
 */
@RestController
@RequestMapping("/data")
public class RotaController {

	@Autowired
	private IRotaService rotaService;

	/**
	 * Obtem todas as rotas
	 * @return
	 */
	@RequestMapping(value = "/getRotas", method = RequestMethod.GET)
	public List<RotaDTO> getRotas() {
		return rotaService.getRotas();
	}
	
	/**
	 * Obtem uma rota pelo ID
	 * 
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/getRota/{id}", method = RequestMethod.GET)
	public RotaDTO getRota(@PathVariable(value = "id") Long id) {
		return rotaService.getRotaById(id);
	}

	/**
	 * Adiciona uma nova rota
	 * 
	 * @param rota
	 * @return
	 */
	@RequestMapping(value = "/addRota", method = RequestMethod.POST)
	public RotaDTO addRota(@Valid @RequestBody(required = true) RotaDTO rota) {
		return rotaService.addRota(rota);
	}
	
	/**
	 * Deleta uma rota pelo seu ID
	 * 
	 * @param id
	 */
	@RequestMapping(value = "/deleteRota/{id}", method = RequestMethod.DELETE)
	public void deleteRota(@PathVariable(value = "id") Long id) {
		rotaService.deleteRotaById(id);
	}
	
	/**
	 * Atualiza uma determinada rota pelo seu ID
	 * 
	 * @param id
	 * @param rota
	 * @return
	 */
	@RequestMapping(value = "/updateRota/{id}", method = RequestMethod.PUT)
	public RotaDTO updateRota(@PathVariable(value = "id") Long id, @Valid @RequestBody(required = true) RotaDTO rota) {
		rota.setId(id);
		
		return rotaService.updateRota(rota);
	}
}
