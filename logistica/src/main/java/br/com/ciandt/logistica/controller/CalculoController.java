package br.com.ciandt.logistica.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.ciandt.logistica.component.ICalculoService;
import br.com.ciandt.logistica.dto.CalculoRotaCustoDTO;
import br.com.ciandt.logistica.dto.ResultadoDTO;

/**
 * Controlador responsavel pelas servico de calculo
 * 
 * @author Rodrigo Florenciano
 *
 */
@RestController
@RequestMapping("/data")
public class CalculoController {

	@Autowired
	private ICalculoService calculoService;
	
	/**
	 * Realiza o calculo a partir do idMalha informado
	 * 
	 * @return
	 */
	@RequestMapping(value = "/calculoRota/{id}", method = RequestMethod.POST)
	public ResultadoDTO calculoRota(@PathVariable(value = "id") Long id, 
			@Valid @RequestBody(required = true) CalculoRotaCustoDTO calculoRotaCustoDTO) {
		
		calculoRotaCustoDTO.setIdMalha(id);
		
		return calculoService.calculoRotaValor(calculoRotaCustoDTO);
	}
	
	/**
	 * Realiza o calculo com o id malha padrão
	 * 
	 * @param calculoRotaCustoDTO
	 * @return
	 */
	@RequestMapping(value = "/calculoRota", method = RequestMethod.POST)
	public ResultadoDTO calculoRotaDefault(@Valid @RequestBody(required = true) 
		CalculoRotaCustoDTO calculoRotaCustoDTO) {
		
		calculoRotaCustoDTO.setIdMalha(null);
		
		return calculoService.calculoRotaValor(calculoRotaCustoDTO);
	}

}
