package br.com.ciandt.logistica.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.ciandt.logistica.component.IMalhaService;
import br.com.ciandt.logistica.dto.MalhaDTO;

/**
 * Controlador das operações do servico de malha
 * 
 * @author Rodrigo Florenciano
 *
 */
@RestController
@RequestMapping("/data")
public class MalhaController {

	@Autowired
	private IMalhaService malhaService;

	/**
	 * Obtem todas as malhas cadastradas
	 * @return
	 */
	@RequestMapping(value = "/getMalhas", method = RequestMethod.GET)
	public List<MalhaDTO> getMalhas() {
		return malhaService.getMalhas();
	}
	
	/**
	 * Obtem a malha pelo seu ID
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/getMalha/{id}", method = RequestMethod.GET)
	public MalhaDTO getMalha(@PathVariable(value = "id") Long id) {
		return malhaService.getMalhaById(id);
	}

	/**
	 * Adiciona uma nova malha
	 * @param malhaDTO
	 * @return
	 */
	@RequestMapping(value = "/addMalha", method = RequestMethod.POST)
	public MalhaDTO addMalha(@Valid @RequestBody(required = true) MalhaDTO malhaDTO) {
		return malhaService.addMalha(malhaDTO);
	}
	
	/**
	 * Remove uma malha pelo seu ID
	 * @param id
	 */
	@RequestMapping(value = "/deleteMalha/{id}", method = RequestMethod.DELETE)
	public void deleteMalha(@PathVariable(value = "id") Long id) {
		malhaService.deleteMalhaById(id);
	}
}
