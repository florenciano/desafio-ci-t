package br.com.ciandt.logistica.dto;

import java.math.BigDecimal;

import javax.validation.constraints.NotNull;

/**
 * Objeto de transferencia de dados para o calculo da rota e custo
 * 
 * @author Rodrigo Florenciano
 *
 */
public class CalculoRotaCustoDTO {
	
	private Long idMalha;

	@NotNull
	private String pontoOrigem;
	
	@NotNull
	private String pontoDestino;
	
	@NotNull
	private BigDecimal autonomia;
	
	@NotNull
	private BigDecimal valorLitro;

	public Long getIdMalha() {
		return idMalha;
	}

	public void setIdMalha(Long idMalha) {
		this.idMalha = idMalha;
	}

	public String getPontoOrigem() {
		return pontoOrigem;
	}

	public void setPontoOrigem(String pontoOrigem) {
		this.pontoOrigem = pontoOrigem;
	}

	public String getPontoDestino() {
		return pontoDestino;
	}

	public void setPontoDestino(String pontoDestino) {
		this.pontoDestino = pontoDestino;
	}

	public BigDecimal getAutonomia() {
		return autonomia;
	}

	public void setAutonomia(BigDecimal autonomia) {
		this.autonomia = autonomia;
	}

	public BigDecimal getValorLitro() {
		return valorLitro;
	}

	public void setValorLitro(BigDecimal valorLitro) {
		this.valorLitro = valorLitro;
	}

	@Override
	public String toString() {
		return "CalculoRotaCustoDTO [idMalha=" + idMalha + ", pontoOrigem=" + pontoOrigem + ", pontoDestino="
				+ pontoDestino + ", autonomia=" + autonomia + ", valorLitro=" + valorLitro + "]";
	}
}
