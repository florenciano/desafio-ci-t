package br.com.ciandt.logistica.dto;

import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

import br.com.ciandt.logistica.domain.Rota;

/**
 * Objeto de transferencia de Rota
 * 
 * @author Rodrigo Florenciano
 *
 */
@XmlRootElement(name="rota")
public class RotaDTO {

	private Long id;
	
	@NotNull
	private String inicioRota;
	
	@NotNull
	private String fimRota;
	
	@NotNull
	private Long distanciaRota;
	
	/**
	 * Constructor Padrão
	 */
	public RotaDTO() {
		super();
	}

	/**
	 * Constructor Padrão
	 * 
	 * @param inicioRota
	 * @param fimRota
	 * @param distanciaRota
	 */
	public RotaDTO(String inicioRota, String fimRota, Long distanciaRota) {
		super();
		this.inicioRota = inicioRota;
		this.fimRota = fimRota;
		this.distanciaRota = distanciaRota;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getInicioRota() {
		return inicioRota;
	}

	public void setInicioRota(String inicioRota) {
		this.inicioRota = inicioRota;
	}

	public String getFimRota() {
		return fimRota;
	}

	public void setFimRota(String fimRota) {
		this.fimRota = fimRota;
	}

	public Long getDistanciaRota() {
		return distanciaRota;
	}

	public void setDistanciaRota(Long distanciaRota) {
		this.distanciaRota = distanciaRota;
	}
	
	/**
	 * Converte DTO para entidade correspondente
	 * @return
	 */
	public Rota _toRotaEntity() {
		return new Rota(inicioRota, fimRota, distanciaRota);
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "RotaDTO [id=" + id + ", inicioRota=" + inicioRota + ", fimRota=" + fimRota + ", distanciaRota="
				+ distanciaRota + "]";
	}
}
