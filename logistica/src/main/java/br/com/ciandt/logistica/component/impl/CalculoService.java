package br.com.ciandt.logistica.component.impl;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.ciandt.logistica.component.ICalculoService;
import br.com.ciandt.logistica.domain.Malha;
import br.com.ciandt.logistica.domain.Rota;
import br.com.ciandt.logistica.dto.CalculoRotaCustoDTO;
import br.com.ciandt.logistica.dto.ResultadoDTO;
import br.com.ciandt.logistica.repository.MalhaRepository;

/**
 * Servico de calculo
 * 
 * @author Rodrigo Florenciano
 *
 */
@Component
public class CalculoService implements ICalculoService {
	
	@Autowired
	private MalhaRepository malhaRepository;

	/*
	 * (non-Javadoc)
	 * @see br.com.ciandt.logistica.component.ICalculoService#calculoRotaValor(br.com.ciandt.logistica.dto.CalculoRotaCustoDTO)
	 */
	@Override
	public ResultadoDTO calculoRotaValor(CalculoRotaCustoDTO calculoRotaCustoDTO) {
		
		
		Malha malha = null;
		if (calculoRotaCustoDTO.getIdMalha() != null) {
			malha = malhaRepository.findById(calculoRotaCustoDTO.getIdMalha());
		}
		else {
			Iterable<Malha> malhaList = malhaRepository.findAll();
			
			if (malhaList != null) {
				malha = malhaRepository.findAll().iterator().next();
			}
		}
		
		if (malha == null) {
			return new ResultadoDTO(null, null, "Malha não encontrada");
		}
		
		List<Rota> rotas = malha.getRotas();
		if (rotas == null || rotas.isEmpty()) {
			return new ResultadoDTO(null, null, "A Malha informada não possui rotas");
		}
		
		List<ResultadoDTO> possiveisRotas = buscaPossiveisRota(calculoRotaCustoDTO.getPontoOrigem(), 
				calculoRotaCustoDTO.getPontoDestino(), rotas);
		
		if (possiveisRotas == null || possiveisRotas.isEmpty()) {
			return new ResultadoDTO(null, null, "A rota informada não foi encontrada na malha");
		}
		
		return calculaMelhorRota(possiveisRotas, calculoRotaCustoDTO.getAutonomia(), calculoRotaCustoDTO.getValorLitro());
	}
	
	/**
	 * Realiza o calculo da melhor rota
	 * 
	 * @param dto
	 * @param autonomia
	 * @param valorLitro
	 * @return
	 */
	public static ResultadoDTO calculaMelhorRota(List<ResultadoDTO> dto, BigDecimal autonomia, BigDecimal valorLitro) {
		Collections.sort(dto);
		
		ResultadoDTO melhorRota = dto.get(0);
		melhorRota.setCusto(valorLitro.multiply(new BigDecimal(melhorRota.getDistanciaTotal())).divide(autonomia, 2, RoundingMode.HALF_UP));
		
		melhorRota.setMensagem("Melhor rota possivel calculada com sucesso");
		
		return melhorRota;
	}
	
	/**
	 * Realiza a busca de possiveis rotas de acordo com os parametros informados 
	 * 
	 * @param rotaInicio
	 * @param rotaFim
	 * @param rotas
	 * @return
	 */
	private List<ResultadoDTO> buscaPossiveisRota(String rotaInicio, String rotaFim, List<Rota> rotas) {
		return buscaPossiveisRota(rotaInicio, rotaFim, rotas, null, null);
	}
	
	/**
	 * Realiza a busca de possiveis rotas de acordo com os parametros informados 
	 * 
	 * @param rotaInicio
	 * @param rotaFim
	 * @param rotas
	 * @param dto
	 * @param dtoLista
	 * @return
	 */
	private List<ResultadoDTO> buscaPossiveisRota(String rotaInicio, String rotaFim, List<Rota> rotas, 
			ResultadoDTO dto, List<ResultadoDTO> dtoLista) {
		
		for (int i = 0; i < rotas.size(); i++) {
			
			if (rotas.get(i).getInicioRota().equalsIgnoreCase(rotaInicio) && rotas.get(i).getFimRota().equalsIgnoreCase(rotaFim)) {
				
				if (dtoLista == null) {
					dtoLista = new ArrayList<ResultadoDTO>();
				}
				
				if (dto == null) {
					dtoLista.add(new ResultadoDTO(rotaInicio + " " + rotaFim, rotas.get(i).getDistanciaRota()));
				}
				else {
					
					if (dtoLista.contains(dto)) {
						dtoLista.get(dtoLista.indexOf(dto)).setRota(dto.getRota() + " " + rotaInicio + " " + rotaFim);
						dtoLista.get(dtoLista.indexOf(dto)).setDistanciaTotal(dto.getDistanciaTotal() + rotas.get(i).getDistanciaRota());
					}
					else {
						dto.setRota(dto.getRota() + " " + rotaInicio + " " + rotaFim);
						dto.setDistanciaTotal(dto.getDistanciaTotal() + rotas.get(i).getDistanciaRota());
						dtoLista.add(dto);
					}
					
					return dtoLista;
				}
			}
			else if (rotas.get(i).getInicioRota().equalsIgnoreCase(rotaInicio)) {
				
				if (dtoLista == null) {
					dtoLista = new ArrayList<ResultadoDTO>();
				}
				
				if (dto == null) {
					dto = new ResultadoDTO();
					dto.setRota(rotaInicio);
					dto.setDistanciaTotal(rotas.get(i).getDistanciaRota());
				}
				
				else {
					dto.setRota(dto.getRota() + " " + rotaInicio);
					dto.setDistanciaTotal(dto.getDistanciaTotal() + rotas.get(i).getDistanciaRota());
				}
				
				dtoLista = buscaPossiveisRota(rotas.get(i).getFimRota(), rotaFim, rotas, dto, dtoLista);
				
				dto = null;
			}
		}
		
		return dtoLista;
	}
}
