package br.com.ciandt.logistica.dto;

import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

import br.com.ciandt.logistica.domain.Malha;
import br.com.ciandt.logistica.domain.Rota;

/**
 * Objeto de transferencia de dadas de malha
 * 
 * @author Rodrigo Florenciano
 *
 */
@XmlRootElement(name="malha")
public class MalhaDTO {
	
	private Long id;

	@NotNull
	private String nome;
	
	@NotNull
	private List<RotaDTO> rotas;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public List<RotaDTO> getRotas() {
		return rotas;
	}

	public void setRotas(List<RotaDTO> rotas) {
		this.rotas = rotas;
	}
	
	/**
	 * Converte DTO para entidade correspondente
	 * 
	 * @return
	 */
	public Malha _toMalhaEntity() {
		ArrayList<Rota> rotas = null;
		
		if (this.rotas != null) {
			rotas = new ArrayList<Rota>();
		
			for (RotaDTO dto: this.rotas) {
				rotas.add(dto._toRotaEntity());
			}
		}
		
		Malha malha = new Malha(nome, rotas);
				
		return malha;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "MalhaDTO [id=" + id + ", nome=" + nome + ", rotas=" + rotas + "]";
	}
}
