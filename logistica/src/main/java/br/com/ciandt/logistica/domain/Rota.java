package br.com.ciandt.logistica.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import br.com.ciandt.logistica.dto.RotaDTO;

/**
 * 
 * Entidade responsavel por mapear a tabela rota
 * 
 * @author Rodrigo Florenciano
 *
 */
@Entity
@Table(uniqueConstraints=
	@UniqueConstraint(columnNames = {"inicioRota", "fimRota", "malha"}))
public class Rota implements Serializable {
	private static final long serialVersionUID = 2890460430972130546L;

	@Id
    @GeneratedValue
    private Long id;

    @Column(nullable = false)
    private String inicioRota;

    @Column(nullable = false)
    private String fimRota;
    
    @Column(nullable = false)
    private Long distanciaRota;
    
    @ManyToOne
    @JoinColumn(name = "malha")
    private Malha malha;
    
    /**
     * Construtor Padrao
     */
    protected Rota() {
    	super();
    }
    
    /**
     * Construtor da entitade Rota
     * 
     * @param inicioRota
     * @param fimRota
     * @param distanciaRota
     */
    public Rota(String inicioRota, String fimRota, Long distanciaRota) {
		super();
		this.inicioRota = inicioRota;
		this.fimRota = fimRota;
		this.distanciaRota = distanciaRota;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getInicioRota() {
		return inicioRota;
	}

	public void setInicioRota(String inicioRota) {
		this.inicioRota = inicioRota;
	}

	public String getFimRota() {
		return fimRota;
	}

	public void setFimRota(String fimRota) {
		this.fimRota = fimRota;
	}

	public Long getDistanciaRota() {
		return distanciaRota;
	}

	public void setDistanciaRota(Long distanciaRota) {
		this.distanciaRota = distanciaRota;
	}
	
	public Malha getMalha() {
		return malha;
	}

	public void setMalha(Malha malha) {
		this.malha = malha;
	}

	/**
	 * Converte a entidade em seu DTO correpondente
	 *  
	 * @return
	 */
	public RotaDTO _toRotaDTO() {
		RotaDTO dto = new RotaDTO();
		dto.setDistanciaRota(distanciaRota);
		dto.setFimRota(fimRota);
		dto.setInicioRota(inicioRota);
		dto.setId(id);
		
		return dto;
	}

	@Override
	public String toString() {
		return "Rota [id=" + id + ", inicioRota=" + inicioRota + ", fimRota=" + fimRota + ", distanciaRota="
				+ distanciaRota + "]";
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((fimRota == null) ? 0 : fimRota.hashCode());
		result = prime * result + ((inicioRota == null) ? 0 : inicioRota.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Rota other = (Rota) obj;
		if (fimRota == null) {
			if (other.fimRota != null)
				return false;
		} else if (!fimRota.equals(other.fimRota))
			return false;
		if (inicioRota == null) {
			if (other.inicioRota != null)
				return false;
		} else if (!inicioRota.equals(other.inicioRota))
			return false;
		return true;
	}
}
