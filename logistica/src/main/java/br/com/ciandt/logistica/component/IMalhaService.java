package br.com.ciandt.logistica.component;

import java.util.List;

import br.com.ciandt.logistica.dto.MalhaDTO;

/**
 * Interface contendo os metodos do servico de malha
 * 
 * @author Rodrigo Florenciano
 *
 */
public interface IMalhaService {
	
	/**
	 * Adiciona malha
	 * 
	 * @param malhaDTO
	 * @return
	 */
	public MalhaDTO addMalha(MalhaDTO malhaDTO);
	
	/**
	 * Obtem uma determinada malha pelo seu ID
	 * 
	 * @return
	 */
	public MalhaDTO getMalhaById(Long id);
	
	/**
	 * Delete uma malha pelo seu ID
	 * 
	 * @return
	 */
	public void deleteMalhaById(Long id);
	
	/**
	 * Update malhas
	 * 
	 * @param malhaDTO
	 * @return
	 */
	public MalhaDTO updateMalha(MalhaDTO malhaDTO);

	/**
	 * Obtem todas as malhas configuradas
	 * 
	 * @return
	 */
	public List<MalhaDTO> getMalhas();
	
	/**
	 * Adiciona as malhas
	 * 
	 * @param malhaDTO
	 */
	public void addMalhas(List<MalhaDTO> malhasDTO);
}
