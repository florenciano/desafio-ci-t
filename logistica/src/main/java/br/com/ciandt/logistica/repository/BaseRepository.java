package br.com.ciandt.logistica.repository;

import java.io.Serializable;

import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.data.repository.Repository;

/**
 * Repositorio base para acesso ao banco de dados
 * 
 * @author Rodrigo Florenciano
 *
 * @param <T>
 * @param <ID>
 */
@NoRepositoryBean
public interface BaseRepository<T, ID extends Serializable> extends Repository<T, ID> {

	/**
	 * Busca entidade pelo seu ID
	 * 
	 * @param id
	 * @return
	 */
	T findById(ID id);

	/**
	 * Salva a entidade
	 * 
	 * @param entity
	 * @return
	 */
	T save(T entity);

	/**
	 * Salva todas as entidades
	 * 
	 * @param entity
	 * @return
	 */
	Iterable<T> save(Iterable<T> entities);

	/**
	 * Busca todas as entidades da tabela
	 * 
	 * @return
	 */
	Iterable<T> findAll();
	
	/**
	 * Remove uma entidade pelo seu ID
	 * 
	 * @param id
	 */
	void removeById(ID id);
}
