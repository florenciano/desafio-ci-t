package br.com.ciandt.logistica.repository;

import br.com.ciandt.logistica.domain.Malha;

public interface MalhaRepository extends BaseRepository<Malha, Long> {
	
	/**
	 * Busca malha pelo nome
	 * 
	 * @param nome
	 * @return
	 */
	Malha findMalhaByNome(String nome);
}
