package br.com.ciandt.logistica.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import br.com.ciandt.logistica.dto.MalhaDTO;
import br.com.ciandt.logistica.dto.RotaDTO;

/**
 * 
 * Entidade responsavel por mapear a tabela malha
 * 
 * @author Rodrigo Florenciano
 *
 */
@Entity
@Table(uniqueConstraints=
	@UniqueConstraint(columnNames = {"nome"}))
public class Malha implements Serializable {
	private static final long serialVersionUID = -8624042911258818062L;

	@Id
    @GeneratedValue
    private Long id;

    @Column(nullable = false)
    private String nome;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private List<Rota> rotas;
    
    /**
     * Construtor Padrao
     */
    protected Malha() {
    	super();
    }

    /**
     * Construtor do objeto Malha
     * 
     * @param nome
     * @param rotas
     */
	public Malha(String nome, List<Rota> rotas) {
		super();
		this.nome = nome;
		this.rotas = rotas;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public List<Rota> getRotas() {
		return rotas;
	}

	public void setRotas(List<Rota> rotas) {
		this.rotas = rotas;
	}
	
	/**
	 * Converte a entidade em seu DTO correpondente
	 *  
	 * @return
	 */
	public MalhaDTO _toMalhaDTO() {
		MalhaDTO dto = new MalhaDTO();
		
		dto.setNome(nome);
		dto.setId(id);
		
		if (rotas != null) {
			dto.setRotas(new ArrayList<RotaDTO>());
			
			for (Rota rota: rotas) {
				dto.getRotas().add(rota._toRotaDTO());
			}
		}
		
		return dto;
	}
}
