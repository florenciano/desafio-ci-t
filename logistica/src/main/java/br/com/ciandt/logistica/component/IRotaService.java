package br.com.ciandt.logistica.component;

import java.util.List;

import br.com.ciandt.logistica.dto.RotaDTO;

/**
 * Interface contendo os metodos do servico de rota
 * 
 * @author Rodrigo Florenciano
 *
 */
public interface IRotaService {
	
	/**
	 * Adiciona rota
	 * 
	 * @param rotaDTO
	 * @return
	 */
	public RotaDTO addRota(RotaDTO rotaDTO);
	
	/**
	 * Obtem uma determinada rota pelo seu ID
	 * 
	 * @return
	 */
	public RotaDTO getRotaById(Long id);
	
	/**
	 * Delete uma rota pelo seu ID
	 * 
	 * @return
	 */
	public void deleteRotaById(Long id);
	
	/**
	 * Update rota
	 * 
	 * @param rotaDTO
	 * @return
	 */
	public RotaDTO updateRota(RotaDTO rotaDTO);

	/**
	 * Obtem todas as rotas configuradas
	 * 
	 * @return
	 */
	public List<RotaDTO> getRotas();
	
	/**
	 * Adiciona as rotas
	 * 
	 * @param rotas
	 */
	public void addRotas(List<RotaDTO> rotasDTO);
}
