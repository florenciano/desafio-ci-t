package br.com.ciandt.logistica.component.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import br.com.ciandt.logistica.component.IMalhaService;
import br.com.ciandt.logistica.domain.Malha;
import br.com.ciandt.logistica.dto.MalhaDTO;
import br.com.ciandt.logistica.repository.MalhaRepository;

/**
 * Servico de malha
 * 
 * @author Rodrigo Florenciano
 *
 */
@Component
public class MalhaService implements IMalhaService {
	
	@Autowired
	private MalhaRepository malhaRepository;

	/*
	 * (non-Javadoc)
	 * @see br.com.ciandt.logistica.component.IMalhaService#addMalha(br.com.ciandt.logistica.dto.MalhaDTO)
	 */
	@Override
	@Transactional
	public MalhaDTO addMalha(MalhaDTO malhaDTO) {
		
		Malha malha = malhaRepository.findMalhaByNome(malhaDTO.getNome());
		if (malha != null) {
			return malha._toMalhaDTO();
		}
		
		return malhaRepository.save(malhaDTO._toMalhaEntity())._toMalhaDTO();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ciandt.logistica.component.IMalhaService#getMalhaById(java.lang.Long)
	 */
	@Override
	public MalhaDTO getMalhaById(Long id) {
		Malha malha = malhaRepository.findById(id);
		
		if (malha == null) {
			return null;
		}
		
		return malha._toMalhaDTO();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ciandt.logistica.component.IMalhaService#deleteMalhaById(java.lang.Long)
	 */
	@Override
	@Transactional
	public void deleteMalhaById(Long id) {
		malhaRepository.removeById(id);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ciandt.logistica.component.IMalhaService#updateMalha(br.com.ciandt.logistica.dto.MalhaDTO)
	 */
	@Override
	@Transactional
	public MalhaDTO updateMalha(MalhaDTO malhaDTO) {
		return malhaRepository.save(malhaDTO._toMalhaEntity())._toMalhaDTO();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ciandt.logistica.component.IMalhaService#getMalhas()
	 */
	@Override
	public List<MalhaDTO> getMalhas() {
		ArrayList<MalhaDTO> dtoList = new ArrayList<MalhaDTO>();
		
		Iterable<Malha> malhas = malhaRepository.findAll();
		for(Malha malha: malhas) {
			dtoList.add(malha._toMalhaDTO());
		}
		
		return dtoList;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ciandt.logistica.component.IMalhaService#addMalhas(java.util.List)
	 */
	@Override
	@Transactional
	public void addMalhas(List<MalhaDTO> malhasDTO) {
		//TODO
	}
}
