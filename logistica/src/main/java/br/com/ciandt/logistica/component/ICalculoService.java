package br.com.ciandt.logistica.component;

import br.com.ciandt.logistica.dto.CalculoRotaCustoDTO;
import br.com.ciandt.logistica.dto.ResultadoDTO;

/**
 * Interface contendo os metodos do servico de calculo
 * 
 * @author Rodrigo Florenciano
 *
 */
public interface ICalculoService {
	
	/**
	 * Realiza o calculo da rota
	 * 
	 * @param calculoRotaCustoDTO
	 * @return
	 */
	public ResultadoDTO calculoRotaValor(CalculoRotaCustoDTO calculoRotaCustoDTO);
}
