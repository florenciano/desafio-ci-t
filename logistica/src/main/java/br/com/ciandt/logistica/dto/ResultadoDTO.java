package br.com.ciandt.logistica.dto;

import java.math.BigDecimal;

/**
 * Objeto de transferencia de dadas de resultado
 * 
 * @author Rodrigo Florenciano
 *
 */
public class ResultadoDTO implements Comparable<ResultadoDTO>{
	
	private String rota;
	private Long distanciaTotal;
	private BigDecimal custo;
	private String mensagem;
	
	/**
	 * Construtor
	 */
	public ResultadoDTO() {
		super();
	}
	
	/**
	 * Construtor padrao
	 * 
	 * @param rota
	 * @param custo
	 * @param mensagem
	 */
	public ResultadoDTO(String rota, BigDecimal custo, String mensagem) {
		super();
		this.rota = rota;
		this.custo = custo;
		this.mensagem = mensagem;
	}

	/**
	 * Construtor padrao
	 * @param rota
	 */
	public ResultadoDTO(String rota) {
		super();
		this.rota = rota;
	}
	
	/**
	 * Construtor padrao
	 * @param rota
	 */
	public ResultadoDTO(String rota, Long distanciaTotal) {
		super();
		this.rota = rota;
		this.distanciaTotal = distanciaTotal;
	}

	public String getRota() {
		return rota;
	}

	public void setRota(String rota) {
		this.rota = rota;
	}

	public BigDecimal getCusto() {
		return custo;
	}

	public void setCusto(BigDecimal custo) {
		this.custo = custo;
	}

	public String getMensagem() {
		return mensagem;
	}

	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}

	public Long getDistanciaTotal() {
		return distanciaTotal;
	}

	public void setDistanciaTotal(Long distanciaTotal) {
		this.distanciaTotal = distanciaTotal;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "ResultadoDTO [rota=" + rota + ", distanciaTotal=" + distanciaTotal + ", custo=" + custo + ", mensagem="
				+ mensagem + "]";
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((rota == null) ? 0 : rota.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ResultadoDTO other = (ResultadoDTO) obj;
		if (rota == null) {
			if (other.rota != null)
				return false;
		} else if (!rota.equals(other.rota))
			return false;
		return true;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(ResultadoDTO o) {
		return this.distanciaTotal.compareTo(o.distanciaTotal);
	}
}
