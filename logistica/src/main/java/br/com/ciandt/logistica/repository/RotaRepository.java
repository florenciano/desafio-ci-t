package br.com.ciandt.logistica.repository;

import br.com.ciandt.logistica.domain.Rota;

public interface RotaRepository extends BaseRepository<Rota, Long> {
	
	/**
	 * Busca uma rota pelo seu inicio e fim
	 * 
	 * @param inicioRota
	 * @param fimRota
	 * @return
	 */
	Rota findByInicioRotaAndFimRota(String inicioRota, String fimRota);
}
