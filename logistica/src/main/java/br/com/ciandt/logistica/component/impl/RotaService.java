package br.com.ciandt.logistica.component.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import br.com.ciandt.logistica.component.IRotaService;
import br.com.ciandt.logistica.domain.Rota;
import br.com.ciandt.logistica.dto.RotaDTO;
import br.com.ciandt.logistica.repository.RotaRepository;

/**
 * Servico de rota
 * 
 * @author Rodrigo Florenciano
 *
 */
@Component
public class RotaService implements IRotaService {
	
	@Autowired
	private RotaRepository rotaRepository;

	/*
	 * (non-Javadoc)
	 * @see br.com.ciandt.logistica.component.IRotaService#getRotas()
	 */
	@Override
	public List<RotaDTO> getRotas() {
		
		Iterable<Rota> rotas = rotaRepository.findAll();
		
		ArrayList<RotaDTO> rotasDTO = new ArrayList<RotaDTO>();
		for (Rota rota: rotas){
			rotasDTO.add(rota._toRotaDTO());
		}
		
		return rotasDTO;
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ciandt.logistica.component.IRotaService#addRotas(java.util.List)
	 */
	@Override
	@Transactional
	public void addRotas(List<RotaDTO> rotasDTO) {
		if (rotasDTO == null) return;
		
		ArrayList<Rota> rotas = new ArrayList<Rota>();
		for (RotaDTO rotaDTO: rotasDTO) {
			rotas.add(rotaDTO._toRotaEntity());
		}
		
		rotaRepository.save(rotas);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ciandt.logistica.component.IRotaService#addRota(br.com.ciandt.logistica.dto.RotaDTO)
	 */
	@Override
	@Transactional
	public RotaDTO addRota(RotaDTO rotaDTO)  {
		if (rotaDTO == null) return null;
		
		Rota rota = rotaRepository.findByInicioRotaAndFimRota(rotaDTO.getInicioRota(), rotaDTO.getFimRota());
		if (rota != null) {
			return rota._toRotaDTO();
		}
		
		return rotaRepository.save(rotaDTO._toRotaEntity())._toRotaDTO();
		
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ciandt.logistica.component.IRotaService#getRotaById(java.lang.Long)
	 */
	@Override
	public RotaDTO getRotaById(Long id) {
		if (id == null) return null;
		
		Rota rota = rotaRepository.findById(id);
		
		if (rota == null) return null;

		return rota._toRotaDTO();
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ciandt.logistica.component.IRotaService#deleteRotaById(java.lang.Long)
	 */
	@Override
	@Transactional
	public void deleteRotaById(Long id) {
		if (id == null) return;
		
		rotaRepository.removeById(id);
	}

	/*
	 * (non-Javadoc)
	 * @see br.com.ciandt.logistica.component.IRotaService#updateRota(br.com.ciandt.logistica.dto.RotaDTO)
	 */
	@Override
	@Transactional
	public RotaDTO updateRota(RotaDTO rotaDTO) {
		if (rotaDTO.getId() == null) return null;
		
		Rota rota = rotaRepository.findById(rotaDTO.getId());
		
		if (rota == null) return null;
		
		rota.setDistanciaRota(rotaDTO.getDistanciaRota());
		rota.setFimRota(rotaDTO.getFimRota());
		rota.setInicioRota(rotaDTO.getInicioRota());
		
		rota = rotaRepository.save(rota);
		
		return rota._toRotaDTO();
	}
}
