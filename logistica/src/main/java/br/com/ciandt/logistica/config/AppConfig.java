package br.com.ciandt.logistica.config;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.orm.jpa.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableAutoConfiguration
@ComponentScan("br.com.ciandt.logistica")
@SpringBootApplication
@EnableTransactionManagement
@EnableJpaRepositories("br.com.ciandt.logistica.repository")
@EntityScan("br.com.ciandt.logistica.domain")
public class AppConfig {

	public static void main(String[] args) {
        SpringApplication.run(AppConfig.class, args);
    }
}
