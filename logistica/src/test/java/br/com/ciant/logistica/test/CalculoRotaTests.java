package br.com.ciant.logistica.test;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import br.com.ciandt.logistica.component.ICalculoService;
import br.com.ciandt.logistica.component.IMalhaService;
import br.com.ciandt.logistica.dto.CalculoRotaCustoDTO;
import br.com.ciandt.logistica.dto.MalhaDTO;
import br.com.ciandt.logistica.dto.ResultadoDTO;
import br.com.ciandt.logistica.dto.RotaDTO;
import br.com.ciant.logistica.test.config.JPATestConfig;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = JPATestConfig.class)
public class CalculoRotaTests {

	@Autowired
	private ICalculoService calculoService;
	
	@Autowired
	private IMalhaService malhaService;
	
	private Long idMalha;
	
	@Before
	public void init () {
		MalhaDTO malha = new MalhaDTO();
		malha.setNome("malha-teste");
		malha.setRotas(new ArrayList<RotaDTO>());
		malha.getRotas().add(new RotaDTO("F", "R", 50L));
		malha.getRotas().add(new RotaDTO("A", "B", 10L));
		malha.getRotas().add(new RotaDTO("B", "D", 15L));
		malha.getRotas().add(new RotaDTO("D", "E", 40L));
		malha.getRotas().add(new RotaDTO("A", "C", 20L));
		malha.getRotas().add(new RotaDTO("C", "D", 30L));
		malha.getRotas().add(new RotaDTO("H", "R", 20L));
		malha.getRotas().add(new RotaDTO("B", "E", 50L));
		malha.getRotas().add(new RotaDTO("G", "H", 10L));
		malha.getRotas().add(new RotaDTO("D", "E", 50L));
		malha.getRotas().add(new RotaDTO("F", "G", 10L));
		
		malha = malhaService.addMalha(malha);
		
		idMalha = malha.getId();
	}
	
	@Test
	public void testCalculaMelhorRotaA() {
		CalculoRotaCustoDTO calculoRotaCustoDTO = new CalculoRotaCustoDTO();
		calculoRotaCustoDTO.setIdMalha(idMalha);
		calculoRotaCustoDTO.setPontoOrigem("A");
		calculoRotaCustoDTO.setPontoDestino("D");
		calculoRotaCustoDTO.setAutonomia(new BigDecimal(10));
		calculoRotaCustoDTO.setValorLitro(new BigDecimal(2.50));
		
		ResultadoDTO resultado = calculoService.calculoRotaValor(calculoRotaCustoDTO);
		
		assertEquals(new BigDecimal(6.25).setScale(2, RoundingMode.HALF_UP), resultado.getCusto());
	}
	
	@Test
	public void testCalculaMelhorRotaB() {
		CalculoRotaCustoDTO calculoRotaCustoDTO = new CalculoRotaCustoDTO();
		calculoRotaCustoDTO.setIdMalha(idMalha);
		calculoRotaCustoDTO.setPontoOrigem("F");
		calculoRotaCustoDTO.setPontoDestino("R");
		calculoRotaCustoDTO.setAutonomia(new BigDecimal(10));
		calculoRotaCustoDTO.setValorLitro(new BigDecimal(2.50));
		
		ResultadoDTO resultado = calculoService.calculoRotaValor(calculoRotaCustoDTO);
		
		System.out.println(resultado);
		
		assertEquals(new BigDecimal(10.00).setScale(2, RoundingMode.HALF_UP), resultado.getCusto());
	}
}
